/**
 * 
 * @author Uczen
 * 
 * W poni�szym kodzie nale�y:
 * - doda� do wszystkich ska�dowych w obu klasach metody set i get
 * - doda� proste menu programu pozwalaj�ce:
 *  > doda� pracownika
 *  > doda� na magazyn towar
 *  > usun�� z magazynu towar
 *  > wy�wietli� towar
 *  > zako�czy� progam
 * - menu powinno by� zap�tlone (np. for (;;){})
 */



public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//tworzenie zmiennej z utworzonej wcze�niej klasy.
		//Aby zmienna zosta�a utworzona i pozwoli�a na
		//zapis/odczyt okre�lonych danych konieczne jest 
		//zainicjalizowanie zmiennej ("powiedzenie" Javie, �e
		//utworzona i wykorzystana klasa ma zosta� przeniesiona
		//do pami�ci operacyjnej - czy ma sta� si� obiektem 
		//(klasa to prototyp, typ danych, obiekt za� to 
		//realny byt w pami�ci g��wnej systemu operacyjnego)
		//Tworzenie obiektu z typu zawsze wykonuje si� specjalnym
		//operatorem new (bez niego obiekt nie powstanie).
		Pracownik pracownik1 = new Pracownik();
		//przypisanie imienia, nazwiska itp.
		//nale�y zwr�ci� uwag�, �e odwo�anie do sk�adowych 
		//obiektu nast�puje poprzez operator sp�jnika "."
		//ma on za zadanie wi�za� nasz obiekt ze "zmienn�", 
		//kt�ra znajduje si� wewn�trz projektowanej klasy.
		pracownik1.imie = "Joanna";
		pracownik1.nazwisko = "Zalewska";
		
		//wy�wietlenie zapisanych zmiennych na konsoli
		System.out.println(pracownik1.imie + " " + pracownik1.nazwisko);
		
		//Wywo�anie przyk�adowej funkcji (metody -> tak� nazw�
		//funkcje nosz� w klasie)
		
//	    pracownik1.wez_z_magazynu();
//	    pracownik1.szukaj_na_magazynie();
	    
	    //metody mog� przyjmowa� parametry tak samo, jak i zwraca�
	    //warto�� jak standardowa funkcja
		
		
		pracownik1.dodaj_pensje(789.76);
		System.out.println(pracownik1.pokaz_saldo());
		
		pracownik1.dodaj_pensje(1245);
		System.out.println(pracownik1.pokaz_saldo());
		
	    
	}

}
