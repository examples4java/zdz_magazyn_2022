/*
 * Java jest j�zykiem niemal w pe�ni obiektowym (z ma�ymi wyj�tkami
 * od obiektowo�ci). W zwi�zku z tym ca�y kod programu
 * standardowo znajduje si� w co najmniej jednej klasie.
 * 
 * Kod w Javie najefektywniej zapisuje si� w konwencji
 * 
 * Plik.java -> zawarto�c pliku to klasa Plik
 * 
 * czyli nazwa pliku automatycznie jest nazw� klasy, kt�ra si� w nim
 * znajduje.
 * 
 * Oryginalne za�o�enie, to �e pojedynczy plik zawiera pojedyncz�
 * klas� (1 plik, 1 klasa).
 * 
 * Oczywi�cie w ramach pojedynczej klasy mamy mo�liwo��
 * tworzenia wi�kszej ilo�ci klas (aczkolwiek nie jest to 
 * oryginalne za�o�enie j�zykowe).
 * 
 * Konwencja nazewnictwa m�wi, �e nazwa klasy zawsze powinna 
 * rozpoczyna� si� du�� liter�. Chodzi o odr�nienie klasy (jako
 * typu) od typ�w prostych.
 * 
 * Klasa jako taka to nic innego jak z�o�ony typ zmiennej, kt�ry
 * mo�emy wykorzystywa� w dalszej cz�ci programu jako typ zmiennej.
 * 
 * Z�o�ono�� objawia si� mo�liwo�ci� zawarcia z klasie wi�cej ni�
 * jednej zmiennej i/lub dowolnej ilo�ci funkcji. Klasy jako takie
 * maj� za zadanie grupowa� zmienne i funkcje w taki spos�b by 
 * mo�liwe by�o ich bezpo�rednie powi�zanie ze sob�. 
 * 
 * Przyk�adowo tworz� klas� Pracownik mo�emy opisa� dowolnego
 * pracownika poprzez okre�lone zmienne/w�a�ciwo�ci:
 * - imie
 * - nazwisko
 * - stanowsko
 * - placa
 * 
 * Pr�cz tego mo�e posiadac okre�lon� "funkcjonalno��":
 * - wez_z_magazynu
 * - oddaj_na_magazyn
 * - szukaj_na_magazynie
 * 
 * Pracownik jako taki mo�e mie� wi�cej "funkcjonalno�ci" 
 * (czynno�ci, kt�e mo�e wykonywa�), jednak to tylko przyk�ad.
 * 
 * Tak zaprojektowana klasa pozwoli tworzy� nowego pracownika
 * w dowolnej cz�ci naszego programu i bez problemu przypisa� mu
 * okre�lone w�a�ciwo�ci.
 */
public class Pracownik {
	String imie,nazwisko,stanowisko;
	//alternatywny zapis zmiennych w klasie.
//	String imie;
//	String nazwisko;
//	String stanowisko;
//	
//	String imie,
//	       nazwisko,
//	       stanowisko;
	long placa; //ilo�� w groszach
	
	void wez_z_magazynu() {
		System.out.println("Bior� z magazynu");
	}
	
	void oddaj_na_magazyn() {
		System.out.println("Oddaj� na magazyn");
	}
	
	void szukaj_na_magazynie() {
		System.out.println("Szukam w magazynie");
	}
	
	///Funkcja ma za zadanie pobiera� pensj� w z�ot�wkach
	///i zamienia� j� na grosze
	void dodaj_pensje(long kwota) {
		//placa=kwota*100;
		placa=Util.zl_grosze(kwota);
	}
	
//	/U�ywamy tzw. przeci��enia funkcji (poprzez typ operatora
//	/parametru wej�ciowego) celem umo�liwienia podania kwoty
//	jako warto�� ca�kowita, jak i zmiennoprzecinkow�
	void dodaj_pensje(double kwota) {
		placa=Util.zl_grosze(kwota);
	}
	
	//w powy�szej oraz poni�szej metodzie u�yto tzw. konwersji
	//typ�w. Oryginalnie zapisan� warto�� double (kwota z metody 
	//powy�szej) mno�ona jest przez warto�� 1oo (zamiana na
	//warto�� ca�kowit�), by nast�pnie warto�� jako typ double
	//zosta� zamieniony JAWNIE na typ long (w kt�rym przechowywana
	//jest placa danej osoby)
	//Poni�ej za� placa zamieniana jest od razy na warto�� double
	//(r�wnie jawnie), by zosta� podzelon�  przez 100. Bez jawnej
	//konwersji warto�� u�amkowa by�a zapominana (typ int/long
	//nie akceptuje warto�ci po przecinku i jest po prostu pomija)
	//Konwersj� jak z przyk�adu poni�ej nie zawsze mo�na zastosowa�.
	//Niekiedy potrzebne s� dodatkowe rozwi�zania j�zykowe
	//(omawiane w p�zniejszym czasie).
	double pokaz_saldo() {
		return (double)placa/100;
	}
}
