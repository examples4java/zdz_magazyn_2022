
public class Util {
	static long zl_grosze(long kwota) {
		return zl_grosze((double)kwota);
	}
	
	//w poni�ej funkcji mamy do czynienia z rozbudowanym
	//narz�dziem konwersji jednego typu na inny
	//Poniewa� ci�g znakowy nie jest typem prostym (a jest
	//klas� zawieraj�c� ci�gi znakowe) tote� zamiana jego
	//na warto�� prost� musi odby� si� poprzez dodatkowe narz�dzie
	//Klas� Double (reprezentuuj�c� obiektow� wersj� prostego typu
	//double). Klasa Double posiada odpowiedni� metod� 
	//zmieniaj�c� warto�� String na double. 	
	//UWAGA! Ka�dy typ prosty posiada sw�j odpowiednik klasowy
	//i posiada takie same narz�dzia konwersji na sw�j typ
	static long zl_grosze(String kwota) {
		//tej linii mo�na nie tworzy�...
		//kwota = kwota.replace(',', '.');
		//poni�ej ekwiwalent - zwr��enie wyniku replace jako 
		//warto�ci parametru dla metody valueOf()
		return zl_grosze(Double.valueOf(kwota.replace(',', '.')));
	}
	
	static long zl_grosze(double kwota) {
    	return (long)(kwota*100);
    }
}
